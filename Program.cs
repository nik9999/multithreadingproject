﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreadingProject
{
    public class ArraySummator
    {
        private int[] data;

        [Params(100000)]
        public int N; 

        [GlobalSetup]
        public void Setup()
        {
            data = new int[N];

            Random r1 = new Random(342);
            for (int j = 0; j < N; j++)
                data[j] =  r1.Next(1, 9);
        }

        [Benchmark]
        public int ComputeSumSingleThread()
        {
            int sum = 0;

            foreach (var item in data)
                sum = sum + item;

            return sum;
        }

        [Benchmark]
        public int ComputeSumParallelWithLinq() => data.AsParallel().Sum();

        [Benchmark]
        public int ComputeSumParallelWithThread()
        {
            var taskCount = 3;
            int sum = 0;

            List<Task<int>> tasks = new List<Task<int>>();

            var splittedArrays = data.Split<int>(taskCount);


            foreach (var item in splittedArrays)
            {
                tasks.Add(Task.Run(() =>
                {
                    return item.Sum();
                }));
            }
            Task.WaitAll(tasks.ToArray());

            tasks.ForEach((item) => { sum = sum + item.Result; } );
           
            return sum;
        }

    }


    //https://www.techiedelight.com/split-an-array-into-chunks-of-specific-size-in-csharp/
    public static class Extensions
    {
        public static IEnumerable<IEnumerable<T>> Split<T>(this T[] arr, int size)
        {
            for (var i = 0; i < arr.Length / size + 1; i++)
            {
                yield return arr.Skip(i * size).Take(size);
            }
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
           var summary = BenchmarkRunner.Run<ArraySummator>();
        }
    }
}
