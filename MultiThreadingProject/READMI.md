``` ini

BenchmarkDotNet=v0.13.1, OS=Windows 10.0.22000
11th Gen Intel Core i7-11700 2.50GHz, 1 CPU, 16 logical and 8 physical cores
.NET SDK=6.0.200
  [Host]     : .NET 5.0.14 (5.0.1422.5710), X64 RyuJIT
  DefaultJob : .NET 5.0.14 (5.0.1422.5710), X64 RyuJIT


```
|                       Method |      N |         Mean |      Error |     StdDev |
|----------------------------- |------- |-------------:|-----------:|-----------:|
|       ComputeSumSingleThread | 100000 |     36.16 μs |   0.111 μs |   0.092 μs |
|   ComputeSumParallelWithLinq | 100000 |     59.90 μs |   0.484 μs |   0.453 μs |
| ComputeSumParallelWithThread | 100000 | 33,561.36 μs | 174.613 μs | 163.333 μs |
